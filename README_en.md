# WeatherTelegramBot

This project is a telegram bot, that can tell the weather in different cities all over the world. His strong sides are a rich set of constructions, that he can interpret, ability to describe the weather not only with words, but also with a picture of a specified city and ability to draw a temperature predictions plot for 10 days ahead.


### General structure
This bot is based on the pyTelegramBotAPI module which has an interface close to that of the origin (telegram's own). It has decorators for everything, which makes the process of creating functions, interacting with users, very easy and pleasant. Of course bot supports both /start and /help commands.

The whole process of answering to the query is the following.
- After message reaches to the bot, he passes it to the **ProcessingModule**.
- There this message is being taken through the **ParseInputsModule** where attempts are made to split the phrase into 2 parts - a time stamp and a location stamp. This part is a non-trivial one; it is based on branching (a lot of ifs) and the pymorphy2 library. Pymorphy2 is a library for messing with the forms of Russian words, which is helpful in obtaining the normal form of cities, (and other parts of inputs) given not in nominative grammatical case.
- If **ParseInputsModule** succeeds, **ProcessingModule** gets a parsed message - that is both a city_name, and a datetime objects. These two quantities form the so-called *request* object which is passed further to the **WeatherForecast** module
- **WeatherForecast** goes to yandex.ru/pogoda... and with **BeautifulSoup** and **requests** modules steals all the weather information (for a specified city and datetime) and stores it in a dictionary, which is later referred to as a *forecast* object. This object is returned back to the **ProcessingModule**
- Then if a user didn't specify a time stamp, **ProcessingModule** passes a *forecast* to the **PlottingModule**, which draws the picture in a known place. If user did specify a time stamp, **ProcessingModule** passes a *forecast* to the **OutputFormattingModule**, where the dictionary with a forecast is translated into the HTML to be displayed in the chat. In the latter case **ProcessingModule** also runs a method from the **RecievePictureModule**, which gets a link to the picture of the specified city with a specified weather.
- Having done these steps, **ProcessingModule** returns to the bot all the instructions - what to tell and what to show and shuts down.
- The bot finishes it all up, executing all the passed instructions.


### Pattern of a phrase that can be asked

1) [ENTRY]? [WHEN] [CITY]'?'? <br/>
2) [ENTRY]? [CITY] [WHEN]'?'?

##### ENTRY (Optional)
- (погода|какая погода|что с погодой|что)

##### WHEN
- (сегодня|завтра|послезавтра|послепослезавтра)
- (в|во) (понедельник|вторник|..|воскресенье|пн|вт|ср|..сб|вс)
- через (0|1|..|9|один|два|три|..|девять)? (дня|дней|день)
- через (одну)? неделю
- dd.mm
- (1-31)(го)? (января|февраля|..декабря|числа)?
- (первого|второго|..|тридцать первого) (января|февраля|..декабря|числа)?

##### city
- (в|во)? (городе)? [ГОРОД(е)] <br/>
(cities of 2 and more words are totaly **ok**) <br/>


### Examples of phrases one can ask the bot:
- Погода в Москве
- Что с погодой в Стокгольме?
- Какая погода в четверг в Астрахани
- Через восемь дней в берлине
- двадцатого мая Париж
- что в берлине послезавтра?
- В Четверг в Казани
- Погода в Санкт-Петербурге 14го
- 17го в Воронеже
- через неделю в Лондоне
- в понедельник в Нижнем Новгороде
