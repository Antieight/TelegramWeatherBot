Я умею предсказывать погоду на 10 дней (включая сегодня)
Чтобы узнать погоду, необходимо указать дату и город


Место нужно писать в формате 
- (в|во)? (городе)? [ГОРОД(е)]

Время -- распознается в следующих форматах:
- (сегодня|завтра|послезавтра|послепослезавтра)
- (в|во) (понедельник|вторник|..|воскресенье|пн|вт|ср|..сб|вс)
- через (0|1|..|9|один|два|три|..|девять)? (дня|дней|день)
- через (одну)? неделю
- dd.mm
- (1-31)(го)? (января|февраля|..декабря|числа)?
- (первого|второго|..|тридцать первого) (января|февраля|..декабря|числа)?

(группы) со знаком вопроса - '(a|b|c)?' 
могут быть опущены (см. примеры)

В начале допускается одна из вводных фраз 
- (погода|какая погода|что с погодой|что)

В конце можно поставить знак вопроса '?'

Порядок указания даты и места не важен.
Можно опустить указание времени, в этом случае будет 
построен график средней температуры на ближайшие 10 дней 
(в каждый день делается 4 предсказания)

Примеры:
- Погода в Москве
- Что с погодой в Стокгольме?
- Какая погода в четверг в Астрахани
- Через восемь дней в берлине
- двадцатого мая Париж
- что в берлине послезавтра?
- В Четверг в Казани
- Погода в Санкт-Петербурге 14го
- 17го в Воронеже

По умолчанию всегда будет выводиться погода в наиболее крупном/популярном городе. 
Чтобы уточнить поиск, введите более полное имя города (с районом, и.т.д)
Например,
- Погода в городе Самара, Октябрьский район завтра
