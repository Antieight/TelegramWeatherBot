import telebot
from modules.ProcessingModule import react_to_message
from time import sleep
import os



# There exists some internal error in this API, so we need to
# wrap it in while True: try-catch. (It can throw an error, but not too often (may be once a day))
while True:
    try:
        bot = telebot.TeleBot("591953777:AAGjypU1tY0RDhWQzWvbeNw5DrYNCfYSFZw")


        @bot.message_handler(commands=['start'])
        def send_welcome(message):
            '''Greeting function. Also sends help message'''
            help_message = 'Привет\n' + open('messages/help.txt').read()
            bot.send_message(message.chat.id, help_message)


        @bot.message_handler(commands=['help'])
        def send_help(message):
            '''Prints help message, that explains, how to use this program.'''
            help_message = open('messages/help.txt').read()
            bot.send_message(message.chat.id, help_message)


        @bot.message_handler(func=lambda m: True)
        def echo_all(message):    
            '''This function connects main logic to TeleBot API and reacts to all the incoming messages.'''
            try:
                for entry in react_to_message(message.text):
                    task = entry[0]
                    args = entry[1:]
                    if task == 'sendErrorMessage':
                        bot.send_message(message.chat.id, args, parse_mode='HTML')
                        raise Exception
                    if task == 'sendMessage':
                        bot.send_message(message.chat.id, args, parse_mode='HTML')
                    if task == 'sendPlot':
                        bot.send_photo(message.chat.id, open(args[0], 'rb'), args[1], parse_mode='HTML')
                        os.remove(args[0])
                    if task == 'sendPhoto':
                        bot.send_photo(message.chat.id, open(args[0], 'rb'), args[1], parse_mode='HTML')
                        os.remove(args[0])
                with open('log.txt', 'a') as log:
                    log.write(f'OK - {message.text}\n')
            except Exception as e:
                with open('log.txt', 'a') as log:
                    log.write(f'FAILED - {message.text}\n')


        bot.polling(none_stop=False, interval=1, timeout=40)
    except Exception:
        pass
