from datetime import timedelta
import matplotlib
matplotlib.use('agg',warn=False, force=True)
import matplotlib.pyplot as plt
from matplotlib import dates
from datetime import date, datetime, time, timedelta


def get_datetime(date, daytime):
    '''Returns a datetime object by a date and a daytime
    
       Parameters:
           date (date) --   date
           daytime (str) -- one of 4 strings -- 'утро', 'день', 'вечер', 'ночь'
       
       Returns:
           datetime object that is, informally speaking, equals to (date + daytime)
           the abovementioned daytimes are taken as 6, 12, 18 and 24 hours correspondinly
    '''
    daytimes = {'утро': 6.,
                'день': 12.,
                'вечер': 18.,
                'ночь': 24.}
    for key in daytimes:
        daytimes[key] = timedelta(daytimes[key] / 24)
    return date + daytimes[daytime]


def do_drawings(times, temperatures, output=None):
    '''takes times and temperatures sequences and plots the dependency

    Parameters:
        times - list of datetimes - corresponding sequence of times
        temperatures - list of floats - corresponding sequence of temperatures
        output=None -- str or None -- if None, draws plot inline
                                      otherwise saves it to a file located at output
    
    Returns None.
    '''
    days = dates.DayLocator()
    hours = dates.HourLocator()
    dfmt = dates.DateFormatter('%b %d')
    fig = plt.figure(figsize=(12,6))
    ax = fig.add_subplot(111)
    ax.xaxis.set_major_locator(days)
    ax.xaxis.set_major_formatter(dfmt)
    ax.xaxis.set_minor_locator(hours)
    plt.plot(times,temperatures,lw=2,color='b')
    plt.grid(0.1)
    if not output:
        plt.show()
    else:
        fig.savefig(output)


def draw_plot(forecast, output=None):
    '''Function, that takes a forecast and draws the dependency of 
       the average temperature over a day on a datetime

    Parameters:
        forecast -- dict -- dictionary with the forecast data
        ouput=None -- str or None -- indicates where output should be directed
                      if None, draws inline, else saves to the file at output

    Returns: 
        times, temperatures -- tuple of two sequences - times and temperatures
'''

    daytimes = ['утро','день','вечер','ночь']
    times = []
    temperatures = []
    for day in forecast:
        for key in forecast[day]:
            if key not in daytimes:
                continue
            dt = datetime.combine(day, datetime.min.time())
            times.append(get_datetime(dt, key))
            temperatures.append(forecast[day][key]['temperature'])
    temperatures = list(map(lambda x: sum([int(el.replace('−', '-')) for el in x]) / len(x), temperatures))
    do_drawings(times, temperatures, output)
    return times, temperatures
