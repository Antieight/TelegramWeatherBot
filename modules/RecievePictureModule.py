import requests as re
from bs4 import BeautifulSoup
import urllib
import json
from random import randint


def shortened_condition(condition):
    '''Makes a keyword to be passed to the google query out of the full-form-condition
       
       Parameters:
           condition -- str -- the whole description of whether at some time
       
       Returns:
           keyword -- str -- somewhat shortened condition to be effectively
                              passed to google to get better pictures
    '''
    if 'дожд' in condition.lower():
        return 'дождь'
    if condition.lower() in ['ясно', 'малооблачно', 'облачно с прояснениями']:
        return 'солнце'
    if 'солн' in condition.lower():
        return 'солнце'
    if 'снег' in condition.lower() or 'снеж' in condition.lower():
        return 'снежный'
    return 'тучи'


def get_link_to_photo(city, condition):
    '''Gets a link to a picture in the web, that represents both specified 
       ciyt and condition. Link choosing process contains randomness.

       Parameters:
           city -- str -- city name, better short for google to be effective
           condition -- str -- the whole description of what is going on 
                               in the city specified at some time

       Returns:
           link -- str -- a link to an image, that shows the city in the condition'''
    condition = shortened_condition(condition)
    def get_soup(url,header):
        return BeautifulSoup(re.get(url,headers=header).text,'html.parser')

    query = city + '+' + condition
    #image_type="ActiOn"
    url="https://www.google.co.in/search?q="+query+"&source=lnms&tbm=isch" # &tbs=isz:m"
    header={'User-Agent':"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"
    }
    soup = get_soup(url, header)


    links = []
    for a in soup.find_all("div",{"class":"rg_meta"}):
        if (str(json.loads(a.text)["ou"]).endswith('jpg') or 
            str(json.loads(a.text)["ou"]).endswith('png')) and (
            True ):#'%' not in str(json.loads(a.text)["ou"])):
            links.append(json.loads(a.text)["ou"])
    a = randint(0,min(4,len(links)))
    link = links[a]
    urllib.request.urlretrieve(link, f"img/{city}_{condition}." + link.split('.')[-1])
    return f"img/{city}_{condition}." + link.split('.')[-1]
