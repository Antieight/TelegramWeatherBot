def get_string_by_forecast_and_day(forecast, day=None):
    '''Makes printable html-message out of forecast, which is originally in form of dictionary
    
     Parameters:
        forecast - dict - the data to be represented. 
                          It is taken from get_weather() function from WeatherForecast module
        day  - datetime - the day for which we need to print the weather.
         It is usually None and then the first key from the forecast is taken

    Returns:
        string, that contains forecast info with a little bit of html to be printed into the chat
    '''
    if day is None:
        day = list(forecast.keys())[0]
    forecast = forecast[day]
    weekdays = '- понедельник вторник среда четверг пятница суббота воскресенье'.split()
    s = '<b>{2} - {0} - {1}</b>\n'.format(day.strftime('%d %b'), weekdays[day.isoweekday()].capitalize(), 
                                          forecast['city_name'])
    months = {
        'January' : 'января',
        'Feburary' : 'февраля',
        'March' : 'марта',
        'April' : 'апреля',
        'May' : 'мая',
        'June': 'июня',
        'July': 'июля',
        'August': 'августа', 
        'September': 'сентября', 
        'October': 'октября', 
        'November':'ноября', 
        'December':'декабря'
    }
    for m in months:
        s = s.replace(m, months[m])
    daytimes = ['утро','день','вечер','ночь']
    daytimes2 = ['Утром','Днем','Вечером','Ночью']
    s += '\n'
    
    for dt, dt2 in zip(daytimes, daytimes2):
        s += '<b>' + dt2.ljust(7) + '</b> будет {}, '.format(forecast[dt]['condition'].lower())
        if len(forecast[dt]['temperature']) > 1:
            s += '<b>({0}..{1})</b>,\n'.format(*forecast[dt]['temperature'])
        else:
            s += '<b>({})</b>,\n'.format(forecast[dt]['temperature'][0])

        if 'feels_like' in forecast[dt]:
            s += 'ощущается как <b>({})</b>\n'.format(forecast[dt]['feels_like'])

        if 'wind_direction' in forecast[dt] and 'wind_strength' in forecast[dt]:
            if forecast[dt]['wind_direction'] is None:
                s += 'Штиль\n'
            else:
                wind_direction = forecast[dt]['wind_direction'][7:].split('-')
                wind_direction = ''.join(map(lambda x: x[0].upper(), wind_direction))
                s += 'Ветер {0} - {1} м/с.\n'.format(wind_direction, forecast[dt]['wind_strength'])

        if 'air_pressure' in forecast[dt]:
            s += 'Давление - {0} мм рт ст\n'.format(forecast[dt]['air_pressure'])
        
        if 'humidity' in forecast[dt]:
            s += 'Влажность - {}\n'.format(forecast[dt]['humidity'])
        s += '\n'
    return s


def make_city(s):
    '''Capitalizes every word in the input string s, 
       and every symbol before '-', making a proper city-name'''
    s = ' '.join(list(map(lambda x: x.capitalize(), s.split())))
    s = '-'.join(list(map(lambda x: x.capitalize(), s.split('-'))))
    return s
