import pymorphy2
from datetime import date, datetime, time, timedelta
# from functools import lru_cache
morph = pymorphy2.MorphAnalyzer()


def norm(word, case=None):
    '''Returns normal form of the word, given the knowledge, that it is 
       in case grammatical_case. Keeps plural form.

       Parameters:
           word -- str -- the word to be normalized
           case -- str(optional) -- prior knowledge of the grammatical case

       Returns:
           word in normalized form (str)
    '''
    if case is None:
        return morph.parse(word)[0].normalized.word
    if case == 'nomn':
        return word.lower()
    for form in ['ADJF', 'NOUN']:
        for option in morph.parse(word):
            if option.tag.case == case and option.tag.POS == form:
                return (option.inflect({option.tag.number, 'nomn'})).word
    # failing to find specified case
    return word.lower()


def parse_when(when):
    '''Takes input string and tries to interpret it as a timestamp

       Parameters:
           when -- str -- the string to be parsed

       Returns:
           False  --   if fails to parse it as a timestamp
           None   --   if when is empty
           Datetime -- if parse was a success'''
    today = (datetime.utcnow() + timedelta(3/24)).date()
    when = when.split()
    if len(when) == 0:
        return None
    normed_when = list(map(norm, when))
    
    # if when is a simple word from the following list we should return 
    options = ['сегодня', 'завтра', 'послезавтра', 'послепослезавтра']
    if normed_when[0] in options and len(when) == 1:
        return today + timedelta(options.index(normed_when[0]))
    
    # may be it's of form 'через два дня' or 'через одну неделю' or 'через день'
    if normed_when[0] == 'через':
        if len(when) < 2:
            return False
        
        # two short options: 'через день' and 'через неделю' 
        if normed_when[1] == 'день' and len(when) == 2:
            return today + timedelta(1)
        if normed_when[1] == 'неделя' and len(when) == 2:
            return today + timedelta(7)
        
        # now options of form 'через [колличество] день/дней/дня'
        # '[колличество]' - is 0,1,2,3..9, or the corresponding text
        if len(when) < 3:
            return False
        options = 'ноль один два три четыре пять шесть семь восемь девять'.split()
        options = {**{str(i):i for i in range(10)}, **{options[i]:i for i in range(10)}}
        if normed_when[1] in options and normed_when[2] == 'день':
            return today + timedelta(options[normed_when[1]])
        
        # now options of form 'через [колличество] недель'
        # as we provide the forecast only for 10 days
        # we should support just one option: 'через одну неделю'
        if normed_when[1] in ['один', '1'] and normed_when[2] == 'неделя':
            return today + timedelta(7)
        
        # else we can't say
        return False
    
    # may be it's of form 'в/во [weekday]'
    if norm(when[0]) == 'в':
        if len(when) != 2:
            return False

        options1 = 'пн вт ср чт пт сб вс'.split()
        options2 = 'понедельник вторник среда четверг пятница суббота воскресение'.split()
        options = {**{options1[i] : i+1 for i in range(7)},
                   **{options2[i] : i+1 for i in range(7)}}
        if normed_when[1] in options:
            weekday = options[normed_when[1]]
            for i in range(7):
                if (today + timedelta(i)).isoweekday() == weekday:
                    return today + timedelta(i)
        return False
    
    # may be it's of form [1-31].[1-12]
    if len(when) == 1:
        guess = when[0].split('.')
        if (len(guess) >= 2 and guess[0].isdigit() and guess[1].isdigit() and
               0 < int(guess[0]) < 31 and 0 < int(guess[1]) < 13):
                    month = int(guess[1])
                    day = int(guess[0])
                    return date(today.year, month, day)


    # now assuming it's of form [1-31] [Month]
    # setting month
    months = 'январь февраль март апрель май июнь мюль август сентябрь октябрь ноябрь декабрь'.split()
    options = {**{months[i]:i+1 for i in range(12)},**{'число': today.month}}
    month = normed_when[-1]
    if month in options:
        month = options[month]
        when = when[:-1]
        normed_when = normed_when[:-1]
    else:
        # may be only day is provided
        # month is assumed to be the current month
        month = today.month
    
    # now setting day
    options = '''- один два три четыре пять шесть семь восемь девять десять одиннадцать 
                 двенадцать тринадцать четырнадцать пятнадцать шестнадцать семнадцать 
                 восемнадцать девятнадцать двадцать двадцатьодин двадцатьдва двадцатьтри 
                 двадцатьчетыре двадцатьпять двадцатьшесть двадцатьсемь двадцатьвосемь 
                 двадцатьдевять тридцать тридцатьодин'''.split()
    for i in range(len(when)):
        parsed = morph.parse(when[i])
        for el in parsed:
            if el.tag.POS == 'ADJF':
                when[i] = el.normalized.word
                break
    preday = ''.join(when)
    if preday in options:
        day = options.index(preday)
    elif preday[:2].isdigit() and preday[2:] == 'го':
        day = int(preday[:2])
    elif preday[:1].isdigit() and preday[1:] == 'го':
        day = int(preday[:1])
    elif preday.isdigit():
        day = int(preday)
    else:
        return False
    
    # if all went ok, we have both month and day! year is assumed to be this year
    try:
        day_to_return = date(today.year, month, day)
    except Exception:
        return False
    if 0 <= (day_to_return - today).days < 10:
        return day_to_return
    else:
        return False


def parse_city(city):
    '''Takes input string and tries to interpret it as a location stamp

       Parameters:
           city -- str -- a string to be parsed to a city

       Returns:
           False if failse to parse
           String - city name - if parse is a success
    '''
    city = city.split()
    normed_city = list(map(norm, city))
    if len(city) >= 3 and normed_city[0] == 'в' and normed_city[1] == 'город':
        grammatical_case = 'nomn'
        city = ' '.join(city[2:])
    elif len(city) >= 2 and normed_city[0] == 'в' and normed_city[1] != 'город':
        grammatical_case = 'loct'
        city = ' '.join(city[1:])
    elif len(city) >= 1 and normed_city[0] != 'в':
        grammatical_case = 'nomn'
        city = ' '.join(city)
    else:
        return False
    
    city_parts = city.split()
    # print(grammatical_case)
    for i in range(len(city_parts)):
        # print('i=',i)
        # print('before',city_parts[i])
        city_parts[i] = norm(city_parts[i], grammatical_case)
        # print('after:',city_parts[i])
    return ' '.join(city_parts)


def startswith_ignorecase(s, t):
    '''return whether s starts with t, ignoring case'''
    if len(t) > len(s):
        return False
    for i in range(len(t)):
        if s[i].lower() != t[i].lower():
            return False
    return True


def parse(s):
    '''Tries to parse s to a request-dict, that is looks for a timestamp and 
       a location stamp. In case of success, return corresponding dict,
       otherwise - return False.
 
       Parameters:
           s -- str -- a string to be parsed
       
       Returns:
           False if fails to understand the input
           Request-Dict (with keys 'city', 'when')
'''
    request = {'city': None, 'when': None}
    entries = 'какая погода_погода_что с погодой_что'.split('_')
    for entry in entries:
        if startswith_ignorecase(s, entry):
            s = s[len(entry):]
            s = s.strip()
            break
    while s[-1] == '?':
        s = s[:-1]
    s = s.split()
    if len(s) == 0:
        return False
    
    if len(s) == 1:
        city = parse_city(s[0])
        if city:
            return {'city' : city, 'when' : None}
    
    city_part_size = 1
    city, when = False, False
    
    while (city is False or when is False) and city_part_size < len(s) + 1:
        # print(city_part_size)
        # print(s[:city_part_size],s[city_part_size:])
        city, when = (parse_city(' '.join(s[:city_part_size])),
                     parse_when(' '.join(s[city_part_size:])))
        city_part_size += 1
    if not city:
        return False
    if when is not None:
        return {'city' : city, 'when' : when}
    mem_city = city
    for i in range(3,0,-1):
        when = parse_when(' '.join(s[:i]))
        city = parse_city(' '.join(s[i:]))
        if when and city:
            return {'when': when, 'city': city}
    return {'city': mem_city, 'when': None}
