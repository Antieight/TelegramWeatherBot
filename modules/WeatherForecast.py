from datetime import date, datetime, time, timedelta
import requests as re
from bs4 import BeautifulSoup
from modules.OutputFormattingModule import make_city


def get_weather(city_name=None, days=None, return_options=False):
    '''Looks up into the web for the weather in a specified city for the specified days 
 
       Parameters:
           city_name -- str -- name of the city
           days -- list of datetimes -- list of days of interest
           return_options -bool- some cities have name-twins, if 
                                 return_options is True, returns options

        Returns:
           if return_options is True, return tuple of the forecast and city_names options
           otherwise, returns only the forecast
    '''
    today = (datetime.utcnow() + timedelta(3/24)).date()
    if days is None:
        days = [today + timedelta(i) for i in range(10)]
    if isinstance(days, int):
        days = [today + timedelta(i) for i in range(min(10, days))]
    if isinstance(days, date):
        days = [days]

    # sending first request to yandex
    weather_page_url = 'http://yandex.ru/pogoda/search?request={}'.format(city_name)
    weather_page_response = re.get(weather_page_url)
    weather_page_html = weather_page_response.text

    # if this city is not unique (which is usually the case), we should also choose the most popular one: 
    # all options are stored in options_bs and can be returned as well
    options = None
    if 'search?request' in weather_page_response.url:
        options_bs =  BeautifulSoup(weather_page_html, 'lxml').find_all('a', class_='place-list__item-name')    
        url = 'http://yandex.ru' + options_bs[0].attrs['href']
        options = [x.next for x in options_bs]
        weather_page_response = re.get(url)
        weather_page_html = weather_page_response.text

    excl = ['область', 'регион', 'и', 'области', 'региона', 'край']
    
    if options is not None:
        city_name = ' '.join(list(map(lambda x: x.capitalize() if x not in excl else x, options[0].split())))
    else:
        city_name = make_city(city_name)

    # now we are one step away from the right page
    further_link = BeautifulSoup(weather_page_html, 'lxml').find('div', class_='forecast-briefly-old__day').a['href']
    weather_page_url = 'http://yandex.ru' + further_link
    weather_page_response = re.get(weather_page_url)
    weather_page_html = weather_page_response.text

    # got correct page
    # parsing it..
    weather_forecast= dict()
    for day in days:
        b = BeautifulSoup(weather_page_html, 'lxml').find_all('dt', attrs={'data-anchor':day.day})[0]
        while b.name != 'dd':
            b = b.next
        this_day_tag = b

        trs = this_day_tag.table.tbody.find_all('tr')
        daytimes = ['утро','день','вечер','ночь']
        weather_by_time = {'city_name' : city_name}
        for i, tr in enumerate(trs):
            tds = tr.find_all('td')
            cur_weather = dict()

            temps = tds[0].find_all('span', class_='temp__value')
            cur_weather['temperature'] = tuple(map(lambda x: x.next, temps))

            cur_weather['condition'] = tds[2].next

            cur_weather['air_pressure'] = tds[3].next

            cur_weather['humidity'] = tds[4].next
            
            if tds[5].find('span', class_='wind-speed'):
                cur_weather['wind_strength'] = tds[5].find_all('span', class_='wind-speed')[0].next
                cur_weather['wind_direction'] = tds[5].find_all('abbr')[0]['title']
            else:
                cur_weather['wind_strength'] = 0
                cur_weather['wind_direction'] = None
            
            cur_weather['feels_like'] = tds[6].find('span', class_='temp__value').text
            weather_by_time[daytimes[i]] = cur_weather

        if b.find('dl', class_='forecast-fields'):
            forecast_fields = list(b.find('dl', class_='forecast-fields').children)
            fields_names = list(map(lambda x:x.next, forecast_fields[::2]))
            fields = forecast_fields[1::2]
            for i in range(len(fields)):
                fields[i] = fields[i].next if i != 0 else fields[i].next + fields[i].next.next.text
            for n, f in zip(fields_names, fields):
                weather_by_time[n] = f
        
        try:
            sunrise_sunset_values = b.find_all('dd', class_='sunrise-sunset__value')
            weather_by_time['sunrise'] = datetime.strptime(sunrise_sunset_values[0].next,'%H:%M').time() 
            weather_by_time['sunset'] = datetime.strptime(sunrise_sunset_values[1].next,'%H:%M').time() 
            weather_by_time['duration'] = sunrise_sunset_values[2].next.replace('\xa0','')
        except Exception:  # happens, when city does not have sunset/sunrise (polar day/night)
            pass
        weather_forecast[day] = weather_by_time
    if return_options:
        return weather_forecast, options
    else:
        return weather_forecast
