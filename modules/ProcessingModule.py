from modules.WeatherForecast import get_weather
from modules.ParseInputsModule import parse
from modules.OutputFormattingModule import get_string_by_forecast_and_day
from datetime import date, datetime, timedelta
from modules.PlottingModule import draw_plot
from modules.RecievePictureModule import get_link_to_photo
from modules.LoggerModule import logger_add_entry, logger_add_line, logger_flush
import pymorphy2
m = pymorphy2.MorphAnalyzer()
# logger_add_entry = logger_flush = lambda x: None
# logger_add_line = lambda x, y: None


def get_response(request):
    '''Wraps get_weather function, takes request in form of dictionary
       with keys ('city', 'when') and passes it to get_weather, gently
       handling all the cases (such as request['when'] is None) 

       Parameters:
           request -- dict -- dictionary with parsed input
        
       Returns:
           response -- dict -- dictionary, that contains the whole forecast
'''
    if 'when' not in request or 'city' not in request:
        return False
    if request['when'] is None:
        return get_weather(request['city'], request['when'], False)
    else:
        return get_weather(request['city'], request['when'], False)


def react_to_message(message):
    '''Accepts message, parses it, gets forecast, forms request and passes it back to the bot

       Parameters:
           message -- str -- the message, that user has sent to the bot

       Returns:
           A list of tuples - a sequence of instructions to be performed by the bot.
                              Each instruction contains both what_to_do (at 0th place)
                              And all the necessary arguments afterwards.
                              For now instructions are 
                              ['sendMessage', 'sendPhoto', 'sendPlot', 'sendErrorMessage']
    '''
    name = str(datetime.utcnow() + timedelta(3/24)) + ' ' + message
    logger_add_entry(name)
    
    request = parse(message)
    logger_add_line(name, f'Parsed message - {request}')
    if not request:
        logger_add_line(name, 'Failed to parse the message. Full stop.')
        logger_flush(name)
        return [('sendErrorMessage', open('messages/error_message.txt').read())]
    try:
        forecast = get_response(request)
    except Exception:
        forecast = False

    if not forecast:
        logger_add_line(name, 'Could not get the forecast from the web. Full stop.')
        logger_flush(name)
        return [('sendErrorMessage', open('messages/error_message.txt').read())]
    else:
        logger_add_line(name, 'The forecast had been successfully obtained')

    if request['when'] is None:
        logger_add_line(name, 'No [WHEN] was specified, drawing the plot.')
        draw_plot(forecast, output='img/' + request['city']+'.png')
        city = forecast[(datetime.utcnow() + timedelta(3/24)).date()]['city_name']
        logger_add_line(name, 'Success.')
        logger_flush(name)
        return [('sendPlot', 'img/' + request['city']+'.png', f'Погода в городе {city}')]
    logger_add_line(name, '[WHEN] was specified, writing the instructions..')
    city = forecast[request['when']]['city_name']
    city_simplified = request['city']
    condition = forecast[request['when']]['день']['condition']
    result = [('sendMessage', get_string_by_forecast_and_day(forecast)),
              ('sendPhoto', get_link_to_photo(city_simplified, condition),
              f'{request["when"]} в городе {city} - {condition}')]
    logger_add_line(name, 'Success')
    logger_flush(name)
    return result
 