log_dict = dict()

def logger_add_entry(name):
    log_dict[name] = ''

def logger_add_line(name, line):
    log_dict[name] += line + '\n'

def logger_flush(name):
    f = open('log.txt', 'a')
    report = f'---\nname is {name}:\n{log_dict[name]}---\n'
    f.write(report)
    # print(report)
    log_dict.pop(name)
    f.close()